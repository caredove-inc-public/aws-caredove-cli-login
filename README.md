# Introduction

This script will allow you to quickly login to AWS CLI, providing your multi-factor authentication token as a variable. This saves a lot of the headache involved with logging into AWS, editing the files would normally need to edit locally every 1-2 days when your session expires.

# Prerequisites

- Docker installed and running
- AWS CLI v2 installed

# AWS Caredove CLI Login

1. Update ARN_OF_MFA in mfa-dev.sh and mfa-dev.sh to the ARN of the multi-factor virtual device you have in your AWS account settings. Update login script at bottom of mfa-dev.sh / mfa-prod.sh to your account ID.

Go here: https://console.aws.amazon.com/iam/home?region=ca-central-1#/users

Click on your username, click to security credentials tab, and copy the ARN next to "Assigned MFA Device".

Do this on both AWS Prod and AWS Dev.

2. Add access keys to AWS Prod and AWS Dev.

3. Save the AWS Access Key ID and Access Key Secret into the credentials file in this repository. Move this credentials file to ~/.aws folder (Default location for your AWS folder).

4. Run the scripts, using `chmod +x *.sh` while your teminal is in the same directory as the scripts to allow permissions to run the scripts locally. Pass in your MFA code from 1Password after the script (`./mfa-dev.sh 123456`)

(Optional) The script will save the session token into your credentials file in your ~/.aws folder. This session token will expire after a couple days, and will require you to run this script again once expired to login. Consider moving these MFA scripts to your `/usr/local/bin` (Mac OSX) folder to allow global access to these scripts, so we can authenticate our AWS CLI in all directories. https://stackoverflow.com/questions/3560326/how-to-make-a-shell-script-global

(Optional) 5. To test both dev and prod are setup correctly and accessible globally, try the below commands in your home directory of your terminal:

`mfa-dev.sh MFA-CODE`

--

`mfa-prod.sh MFA-CODE`

This allows us to check both scripts correctly authenticate with AWS, in both our Dev and Production accounts. To test your permissions (Assuming your account should have access to ECS clusters), run:

`aws ecs list-clusters`

This should return a list of all clusters in the respective accounts.